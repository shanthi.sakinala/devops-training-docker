# build
FROM node:alpine as builder
WORKDIR /app
COPY package.json package-lock.json ./
ENV CI=1
RUN npm ci
#RUN npm install
COPY . .
RUN npm run build

# deploy
FROM nginx
EXPOSE 80
COPY ./conf/default.conf /etc/nginx/conf.d/default.conf

RUN rm -rf /usr/share/nginx/html/*

COPY --from=builder /app/dist/ /usr/share/nginx/html
RUN service nginx start
#RUN service nginx status
#ENTRYPOINT ["nginx","-g", "daemon off;"]
